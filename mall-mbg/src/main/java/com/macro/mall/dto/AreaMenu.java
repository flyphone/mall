package com.macro.mall.dto;

import com.macro.mall.model.Area;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @Auther: shenzhuan
 * @Date: 2019/3/30 14:04
 * @Description:
 */
public class AreaMenu  extends Area{
    @Getter
    @Setter
    private List<Area> children;
}
