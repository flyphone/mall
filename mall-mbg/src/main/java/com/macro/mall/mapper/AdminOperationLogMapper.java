package com.macro.mall.mapper;


import com.macro.mall.model.SystemOperationLog;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AdminOperationLogMapper {
    int deleteByPrimaryKey(Long id);

    int insertSelective(SystemOperationLog record);

    SystemOperationLog selectByPrimaryKey(Long id);


    Integer queryTotalRecord(@Param("keyword") String keyword, @Param("userName") String userName, @Param("serviceName") String serviceName,
                             @Param("method") String method, @Param("startTime") String startTime, @Param("endTime") String endTime);

    List<SystemOperationLog> queryList(SystemOperationLog adminLog);

    List<SystemOperationLog> queryExportList(@Param("keyword") String keyword, @Param("userName") String userName, @Param("serviceName") String serviceName,
                                      @Param("method") String method, @Param("startTime") String startTime, @Param("endTime") String endTime);
}