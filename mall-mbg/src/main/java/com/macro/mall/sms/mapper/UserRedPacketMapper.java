package com.macro.mall.sms.mapper;

import com.macro.mall.sms.model.SmsUserRedPacket;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 用户红包
 * @author zscat
 * @email 951449465@qq.com
 * @date 2019-04-05 16:20:37
 */

public interface UserRedPacketMapper {

	SmsUserRedPacket get(Integer id);
	
	List<SmsUserRedPacket> list(SmsUserRedPacket userRedPacket);

    int count(SmsUserRedPacket userRedPacket);
	
	int save(SmsUserRedPacket userRedPacket);
	
	int update(SmsUserRedPacket userRedPacket);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);

	int insertList(@Param("list") List<SmsUserRedPacket> list);

    SmsUserRedPacket listUserRedOne(Integer id);
	int countOne(@Param("redPacketId") Integer redPacketId,@Param("userId") Long userId);

}
