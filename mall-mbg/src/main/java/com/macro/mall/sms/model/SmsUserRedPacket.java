package com.macro.mall.sms.model;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;



/**
 * 用户红包
 * 
 * @author zscat
 * @email 951449465@qq.com
 * @date 2019-04-05 16:20:37
 */
@Data
public class SmsUserRedPacket implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//用户红包编号
	private Integer id;
	//红包编号
	private Integer redPacketId;
	//发红包用户的编号
	private Long adminId;
	//抢红包用户的编号
	private Long userId;
	//抢到的红包金额
	private BigDecimal amount;
	//抢红包时间
	private Date grabTime;
	//备注
	private String note;


}
