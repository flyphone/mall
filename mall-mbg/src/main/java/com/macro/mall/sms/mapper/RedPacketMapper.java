package com.macro.mall.sms.mapper;

import com.macro.mall.sms.model.SmsRedPacket;

import java.util.List;


/**
 * 红包
 * @author zscat
 * @email 951449465@qq.com
 * @date 2019-04-05 16:20:35
 */

public interface RedPacketMapper {

	SmsRedPacket get(Integer id);
	
	List<SmsRedPacket> list(SmsRedPacket redPacket);

    int count(SmsRedPacket redPacket);
	
	int save(SmsRedPacket redPacket);
	
	int update(SmsRedPacket redPacket);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);


}
