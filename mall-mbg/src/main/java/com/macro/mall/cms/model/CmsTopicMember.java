package com.macro.mall.cms.model;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author zscat
 * @email 951449465@qq.com
 * @date 2019-04-04 14:04:34
 */
public class CmsTopicMember implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//编号
	private Long id;
	//会员编号
	private Long memberId;
	//会员
	private String username;
	//状态
	private String status ; // 1 通过 2 不通过
	//活动编号
	private Long topicId;
	//创建时间
	private Date createTime;

	/**
	 * 设置：编号
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：编号
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：会员编号
	 */
	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}
	/**
	 * 获取：会员编号
	 */
	public Long getMemberId() {
		return memberId;
	}
	/**
	 * 设置：会员
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * 获取：会员
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * 设置：状态
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * 获取：状态
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * 设置：活动编号
	 */
	public void setTopicId(Long topicId) {
		this.topicId = topicId;
	}
	/**
	 * 获取：活动编号
	 */
	public Long getTopicId() {
		return topicId;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
