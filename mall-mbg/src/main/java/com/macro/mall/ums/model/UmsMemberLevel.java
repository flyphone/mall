package com.macro.mall.ums.model;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;



/**
 * 会员等级表
 * 
 * @author zscat
 * @email 951449465@qq.com
 * @date 2019-04-01 18:41:04
 */
@Data
public class UmsMemberLevel implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;

	private String name;

	private Integer growthPoint;

	/**
	 * 是否为默认等级：0->不是；1->是
	 *
	 * @mbggenerated
	 */
	private Integer defaultStatus;

	/**
	 * 免运费标准
	 *
	 * @mbggenerated
	 */
	private BigDecimal freeFreightPoint;

	/**
	 * 每次评价获取的成长值
	 *
	 * @mbggenerated
	 */
	private Integer commentGrowthPoint;

	/**
	 * 是否有免邮特权
	 *
	 * @mbggenerated
	 */
	private Integer priviledgeFreeFreight;

	/**
	 * 是否有签到特权
	 *
	 * @mbggenerated
	 */
	private Integer priviledgeSignIn;

	/**
	 * 是否有评论获奖励特权
	 *
	 * @mbggenerated
	 */
	private Integer priviledgeComment;

	/**
	 * 是否有专享活动特权
	 *
	 * @mbggenerated
	 */
	private Integer priviledgePromotion;

	/**
	 * 是否有会员价格特权
	 *
	 * @mbggenerated
	 */
	private Integer priviledgeMemberPrice;

	/**
	 * 是否有生日特权
	 *
	 * @mbggenerated
	 */
	private Integer priviledgeBirthday;

	private String note;
	//
	private Integer articlecount;
	//是否为默认等级：0->不是；1->是
	private Integer goodscount;
	//免运费标准
	private BigDecimal price;

}