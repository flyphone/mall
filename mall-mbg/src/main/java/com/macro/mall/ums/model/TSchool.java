package com.macro.mall.ums.model;

import java.io.Serializable;



/**
 * 
 * 
 * @author zscat
 * @email 951449465@qq.com
 * @date 2019-04-01 18:43:58
 */
public class TSchool implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private Long id;
	//精度
	private String longitude;
	//纬度
	private String latitude;
	//名称
	private String name;
	//省
	private String province;
	//市
	private String city;
	//区
	private String region;
	//地址
	private String address;
	//1热门
	private Integer hot;
	//211
	private Integer is211;
	//985
	private Integer is985;

	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：精度
	 */
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	/**
	 * 获取：精度
	 */
	public String getLongitude() {
		return longitude;
	}
	/**
	 * 设置：纬度
	 */
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	/**
	 * 获取：纬度
	 */
	public String getLatitude() {
		return latitude;
	}
	/**
	 * 设置：名称
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：名称
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：省
	 */
	public void setProvince(String province) {
		this.province = province;
	}
	/**
	 * 获取：省
	 */
	public String getProvince() {
		return province;
	}
	/**
	 * 设置：市
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * 获取：市
	 */
	public String getCity() {
		return city;
	}
	/**
	 * 设置：区
	 */
	public void setRegion(String region) {
		this.region = region;
	}
	/**
	 * 获取：区
	 */
	public String getRegion() {
		return region;
	}
	/**
	 * 设置：地址
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * 获取：地址
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * 设置：1热门
	 */
	public void setHot(Integer hot) {
		this.hot = hot;
	}
	/**
	 * 获取：1热门
	 */
	public Integer getHot() {
		return hot;
	}
	/**
	 * 设置：211
	 */
	public void setIs211(Integer is211) {
		this.is211 = is211;
	}
	/**
	 * 获取：211
	 */
	public Integer getIs211() {
		return is211;
	}
	/**
	 * 设置：985
	 */
	public void setIs985(Integer is985) {
		this.is985 = is985;
	}
	/**
	 * 获取：985
	 */
	public Integer getIs985() {
		return is985;
	}
}
