package com.macro.mall.portal.controller;

import com.macro.mall.annotation.IgnoreAuth;
import com.macro.mall.annotation.SysLog;
import com.macro.mall.dto.CommonResult;
import com.macro.mall.dto.PmsProductAndGroup;
import com.macro.mall.model.*;
import com.macro.mall.portal.cms.service.SubjectCategoryService;
import com.macro.mall.portal.cms.service.SubjectCommentService;
import com.macro.mall.portal.cms.service.SubjectService;
import com.macro.mall.portal.service.PmsProductAttributeCategoryService;
import com.macro.mall.portal.service.PmsProductCategoryService;
import com.macro.mall.portal.service.PmsProductService;
import com.macro.mall.portal.sms.service.GroupService;
import com.macro.mall.portal.ums.service.MemberLevelService;
import com.macro.mall.portal.util.DateUtils;
import com.macro.mall.sms.mapper.GroupMapper;
import com.macro.mall.sms.model.SmsGroup;
import com.macro.mall.ums.model.UmsMemberLevel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Auther: shenzhuan
 * @Date: 2019/4/2 15:02
 * @Description:
 */
@RestController
@Api(tags = "CmsController", description = "商品关系管理")
@RequestMapping("/api/pms")
public class PmsController extends ApiBaseAction{

    @Resource
    private GroupService groupService;
    @Resource
    private MemberLevelService memberLevelService;
    @Resource
    private PmsProductService pmsProductService;
    @Resource
    private PmsProductAttributeCategoryService productAttributeCategoryService;
    @Resource
    private PmsProductCategoryService productCategoryService;
    @Resource
    private GroupMapper groupMapper;

    @Resource
    private SubjectCategoryService subjectCategoryService;
    @Resource
    private SubjectService subjectService;
    @Resource
    private SubjectCommentService commentService;

    @SysLog(MODULE = "pms", REMARK = "查询商品详情信息")
    @IgnoreAuth
    @GetMapping(value = "/goods/detail")
    @ApiOperation(value = "查询商品详情信息")
    public Object queryProductDetail(@RequestParam(value = "id", required = false, defaultValue = "0") Long id) {
        PmsProductAndGroup productResult = pmsProductService.get(id);
        Map<String ,Object> map = new HashMap<>();
        SmsGroup group = groupMapper.getByGoodsId(id);
        if (group!=null){
            Date endTime = DateUtils.convertStringToDate(DateUtils.addHours(group.getEndTime(),group.getHours()),"yyyy-MM-dd HH:mm:ss");
            Long nowT = System.currentTimeMillis();
            if (group!=null && nowT>group.getStartTime().getTime() && nowT<endTime.getTime()){
                map.put("group",group);
                map.put("isGroup",1);
            }else{
                map.put("isGroup",2);
            }
        }

        map.put("goods",productResult);
        return new com.macro.mall.portal.domain.CommonResult().success(map);
    }
    @SysLog(MODULE = "pms", REMARK = "查询商品列表")
    @IgnoreAuth
    @ApiOperation(value = "查询商品列表")
    @GetMapping(value = "/goods/list")
    public Object goodsList(PmsProduct product,
                              @RequestParam(value = "pageSize", required = false, defaultValue = "5") Integer pageSize,
                              @RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum) {
        product.setPublishStatus(1);
        product.setVerifyStatus(1);
        List<PmsProduct> list = pmsProductService.list(product,pageSize,pageNum);
        return new CommonResult().pageSuccess(list);
    }

    @SysLog(MODULE = "pms", REMARK = "查询商品分类列表")
    @IgnoreAuth
    @ApiOperation(value = "查询商品分类列表")
    @GetMapping(value = "/productCategory/list")
    public Object productCategoryList(PmsProductCategory productCategory,
                          @RequestParam(value = "pageSize", required = false, defaultValue = "5") Integer pageSize,
                          @RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum) {
        List<CmsSubjectCategory> list = productCategoryService.list(productCategory,pageSize,pageNum);
        return new CommonResult().pageSuccess(list);
    }

    @SysLog(MODULE = "pms", REMARK = "查询商品属性分类列表")
    @IgnoreAuth
    @ApiOperation(value = "查询商品属性分类列表")
    @GetMapping(value = "/productAttributeCategory/list")
    public Object productAttributeCategoryList(PmsProductAttributeCategory productAttributeCategory,
                              @RequestParam(value = "pageSize", required = false, defaultValue = "5") Integer pageSize,
                              @RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum) {
        List<CmsSubjectComment> list = productAttributeCategoryService.list(productAttributeCategory,pageSize,pageNum);
        return new CommonResult().pageSuccess(list);
    }


    @ApiOperation("创建商品")
    @SysLog(MODULE = "pms", REMARK = "创建商品")
    @RequestMapping(value = "/createGoods")
    public Object createGoods(PmsProduct productParam) {
        CommonResult commonResult;
        UmsMember member = this.getCurrentMember();
        if (member.getMemberLevelId()>0){
            UmsMemberLevel memberLevel = memberLevelService.getMemberLevel(member.getMemberLevelId());
            PmsProduct newSubject = new PmsProduct();
            newSubject.setSupplyId(member.getId());
            newSubject.setPublishStatus(1);
            newSubject.setVerifyStatus(1);
            List<PmsProduct> subjects = pmsProductService.list(newSubject,1,1000);
            if (subjects!=null && subjects.size()>memberLevel.getGoodscount()){
                commonResult = new CommonResult().failed("你今天已经有发"+memberLevel.getGoodscount()+"个商品");
                return commonResult;
            }
        }
        productParam.setSupplyId(member.getId());
        productParam.setCreateTime(new Date());
        int count = pmsProductService.create(productParam);
        if (count > 0) {
            return new com.macro.mall.dto.CommonResult().success(count);
        } else {
            return new com.macro.mall.dto.CommonResult().failed();
        }
    }

    @SysLog(MODULE = "pms", REMARK = "根据pid查询区域")
    @ApiOperation("根据pid查询区域")
    @RequestMapping(value = "/getAreaByPid", method = RequestMethod.GET)
    public Object getAreaByPid(@RequestParam(value = "pid", required = false, defaultValue = "0") Long pid) {
        List<Area> list = pmsProductService.getByPid(pid);
        return new com.macro.mall.dto.CommonResult().success(list);
    }
}
