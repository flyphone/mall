package com.macro.mall.portal.controller;

import com.macro.mall.annotation.IgnoreAuth;
import com.macro.mall.annotation.SysLog;
import com.macro.mall.dto.CommonResult;
import com.macro.mall.model.OmsOrder;
import com.macro.mall.model.UmsMember;
import com.macro.mall.portal.cms.service.SubjectCategoryService;
import com.macro.mall.portal.cms.service.SubjectCommentService;
import com.macro.mall.portal.cms.service.SubjectService;
import com.macro.mall.portal.service.OmsOrderService;
import com.macro.mall.portal.sms.service.GroupService;
import com.macro.mall.portal.sms.vo.GroupAndOrderVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Auther: shenzhuan
 * @Date: 2019/4/2 15:02
 * @Description:
 */
@RestController
@Api(tags = "OmsController", description = "订单管理系统")
@RequestMapping("/api/oms")
public class OmsController extends ApiBaseAction{

    @Resource
    private GroupService groupService;
    @Resource
    private OmsOrderService orderService;
    @Resource
    private SubjectCategoryService subjectCategoryService;
    @Resource
    private SubjectService subjectService;
    @Resource
    private SubjectCommentService commentService;

    @IgnoreAuth
    @SysLog(MODULE = "oms", REMARK = "查询订单列表")
    @ApiOperation(value = "查询订单列表")
    @GetMapping(value = "/order/list")
    public Object orderList(OmsOrder order,
                              @RequestParam(value = "pageSize", required = false, defaultValue = "5") Integer pageSize,
                              @RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum) {
        List<OmsOrder> list = orderService.listOrder(order,pageSize,pageNum);
        return new CommonResult().pageSuccess(list);
    }


    /**
     * 提交订单
     * @param orderParam
     * @return
     */
    @ApiOperation("商品详情预览订单")
    @SysLog(MODULE = "order", REMARK = "商品详情预览订单")
    @RequestMapping(value = "/preOrder")
    public Object preOrder(GroupAndOrderVo orderParam) {
        UmsMember member = this.getCurrentMember();
        orderParam.setMemberId(member.getId());
        orderParam.setName(member.getNickname());
        return groupService.preOrder(orderParam);
    }
    /**
     * 提交订单
     * @param orderParam
     * @return
     */
    @ApiOperation("商品详情生成订单")
    @SysLog(MODULE = "order", REMARK = "商品详情生成订单")
    @RequestMapping(value = "/bookOrder")
    public Object bookOrder(GroupAndOrderVo orderParam) {
        UmsMember member = this.getCurrentMember();
        return groupService.generateOrder(orderParam,member);
    }
}
