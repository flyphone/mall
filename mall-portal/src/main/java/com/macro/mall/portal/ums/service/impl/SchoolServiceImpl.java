package com.macro.mall.portal.ums.service.impl;

import com.github.pagehelper.PageHelper;
import com.macro.mall.mapper.SchoolMapper;
import com.macro.mall.portal.ums.service.SchoolService;
import com.macro.mall.ums.model.TSchool;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 商品品牌Service实现类
 * Created by macro on 2018/4/26.
 */
@Service
public class SchoolServiceImpl implements SchoolService {
    @Resource
    private SchoolMapper schoolMapper;
    


    @Override
    public int createSchool(TSchool school) {
        return schoolMapper.save(school);
    }

    @Override
    public int updateSchool(Long id, TSchool school) {
        school.setId(id);
        return schoolMapper.update(school);
    }

    @Override
    public int deleteSchool(Long id) {
        return schoolMapper.remove(id);
    }


    @Override
    public List<TSchool> listSchool(TSchool school, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        return schoolMapper.list(school);

    }

    @Override
    public TSchool getSchool(Long id) {
        return schoolMapper.get(id);
    }

   
}
