package com.macro.mall.sms.service;

import com.macro.mall.sms.model.SmsUserRedPacket;


import java.util.List;

/**
 * 用户红包
 *
 * @author zscat
 * @email 951449465@qq.com
 * @date 2019-04-05 16:20:37
 */
public interface UserRedPacketService {


    int createUserRedPacket(SmsUserRedPacket userRedPacket);
    
    int updateUserRedPacket(Integer id, SmsUserRedPacket userRedPacket);

    int deleteUserRedPacket(Integer id);



    List<SmsUserRedPacket> listUserRedPacket(SmsUserRedPacket userRedPacket, int pageNum, int pageSize);

    SmsUserRedPacket getUserRedPacket(Integer id);

   
}
