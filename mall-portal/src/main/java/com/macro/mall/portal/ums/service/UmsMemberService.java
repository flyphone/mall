package com.macro.mall.portal.ums.service;

import com.macro.mall.model.UmsMember;
import com.macro.mall.model.UserFormId;
import com.macro.mall.portal.domain.CommonResult;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * 商品品牌Service
 * https://gitee.com/zscat-platform/mall on 2018/4/26.
 */
public interface UmsMemberService {
    List<UmsMember> listAllMember();

    int createMember(UmsMember UmsMember);

    @Transactional
    int updateMember(Long id, UmsMember UmsMember);

    int deleteMember(Long id);

    int deleteMember(List<Long> ids);

    List<UmsMember> listMember(String keyword, int pageNum, int pageSize);

    UmsMember getMember(Long id);

    /**
     * 根据用户名获取会员
     */
    UmsMember getByUsername(String username);

    /**
     * 根据会员编号获取会员
     */
    UmsMember getById(Long id);

    /**
     * 用户注册
     */
    @Transactional
    CommonResult register(String username, String password, String telephone, String authCode);

    /**
     * 生成验证码
     */
    CommonResult generateAuthCode(String telephone);

    /**
     * 修改密码
     */
    @Transactional
    CommonResult updatePassword(String telephone, String password, String authCode);

    /**
     * 获取当前登录会员
     */
    UmsMember getCurrentMember();

    /**
     * 根据会员id修改会员积分
     */
    void updateIntegration(Long id, Integer integration);

    public UmsMember queryByOpenId(String openId);

    Object loginByWeixin(HttpServletRequest req);

    Map<String, Object> login(String username, String password);

    String refreshToken(String token);

    Object register(UmsMember umsMember);

    /**
     * 增加市场推送FormId信息
     * @param entity
     */
    public void insert(UserFormId entity);

    /**
     * 根据传入的formId，查询是否存在
     * @param formId
     * @return
     */
    public Boolean exists(String formId);
}
