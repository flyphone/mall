package com.macro.mall.portal.ums.service;

import com.macro.mall.ums.model.UmsMemberBlanceLog;

import java.util.List;

/**
 * 
 *
 * @author zscat
 * @email 951449465@qq.com
 * @date 2019-04-01 18:44:20
 */
public interface MemberBlanceLogService {


    int createMemberBlanceLog(UmsMemberBlanceLog memberBlanceLog);
    
    int updateMemberBlanceLog(Long id, UmsMemberBlanceLog memberBlanceLog);

    int deleteMemberBlanceLog(Long id);



    List<UmsMemberBlanceLog> listMemberBlanceLog(UmsMemberBlanceLog memberBlanceLog, int pageNum, int pageSize);

    UmsMemberBlanceLog getMemberBlanceLog(Long id);

   
}
