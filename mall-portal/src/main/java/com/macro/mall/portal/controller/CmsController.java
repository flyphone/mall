package com.macro.mall.portal.controller;

import com.macro.mall.annotation.IgnoreAuth;
import com.macro.mall.annotation.SysLog;
import com.macro.mall.dto.CommonResult;
import com.macro.mall.model.CmsSubject;
import com.macro.mall.model.CmsSubjectCategory;
import com.macro.mall.model.CmsSubjectComment;
import com.macro.mall.model.UmsMember;
import com.macro.mall.portal.cms.service.SubjectCategoryService;
import com.macro.mall.portal.cms.service.SubjectCommentService;
import com.macro.mall.portal.cms.service.SubjectService;
import com.macro.mall.portal.ums.service.MemberLevelService;
import com.macro.mall.ums.model.UmsMemberLevel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Auther: shenzhuan
 * @Date: 2019/4/2 15:02
 * @Description:
 */
@RestController
@Api(tags = "CmsController", description = "内容关系管理")
@RequestMapping("/api/cms")
public class CmsController  extends ApiBaseAction{

    @Resource
    private SubjectCategoryService subjectCategoryService;
    @Resource
    private SubjectService subjectService;
    @Resource
    private SubjectCommentService commentService;
    @Resource
    private MemberLevelService memberLevelService;

    @IgnoreAuth
    @SysLog(MODULE = "cms", REMARK = "查询文章列表")
    @ApiOperation(value = "查询文章列表")
    @GetMapping(value = "/subject/list")
    public Object subjectList(CmsSubject subject,
                              @RequestParam(value = "pageSize", required = false, defaultValue = "5") Integer pageSize,
                              @RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum) {
        List<CmsSubject> list = subjectService.listSubject(subject,pageSize,pageNum);
        return new CommonResult().pageSuccess(list);
    }
    @SysLog(MODULE = "cms", REMARK = "查询文章分类列表")
    @IgnoreAuth
    @ApiOperation(value = "查询文章分类列表")
    @GetMapping(value = "/subjectCategory/list")
    public Object cateList(CmsSubjectCategory subjectCategory,
                          @RequestParam(value = "pageSize", required = false, defaultValue = "5") Integer pageSize,
                          @RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum) {
        List<CmsSubjectCategory> list = subjectCategoryService.listSubjectCategory(subjectCategory,pageSize,pageNum);
        return new CommonResult().pageSuccess(list);
    }

    @SysLog(MODULE = "cms", REMARK = "查询文章评论列表")
    @IgnoreAuth
    @ApiOperation(value = "查询文章评论列表")
    @GetMapping(value = "/subjectComment/list")
    public Object subjectList(CmsSubjectComment subjectComment,
                              @RequestParam(value = "pageSize", required = false, defaultValue = "5") Integer pageSize,
                              @RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum) {
        List<CmsSubjectComment> list = commentService.listSubjectComment(subjectComment,pageSize,pageNum);
        return new CommonResult().pageSuccess(list);
    }
    @SysLog(MODULE = "cms", REMARK = "创建文章")
    @ApiOperation(value = "创建文章")
    @RequestMapping(value = "/createSubject")
    @ResponseBody
    public Object createSubject(CmsSubject subject, BindingResult result) {
        CommonResult commonResult;
        UmsMember member = this.getCurrentMember();
        if (member.getMemberLevelId()>0){
            UmsMemberLevel memberLevel = memberLevelService.getMemberLevel(member.getMemberLevelId());
            CmsSubject newSubject = new CmsSubject();
            newSubject.setMemberId(member.getId());
            List<CmsSubject> subjects = subjectService.listSubject(newSubject,1,1000);
            if (subjects!=null && subjects.size()>memberLevel.getArticlecount()){
                commonResult = new CommonResult().failed("你今天已经有发"+memberLevel.getArticlecount()+"篇文章");
                return commonResult;
            }
        }
        subject.setMemberId(member.getId());
        int count = subjectService.createSubject(subject);
        if (count == 1) {
            commonResult = new CommonResult().success(count);
        } else {
            commonResult = new CommonResult().failed();
        }
        return commonResult;
    }


}
