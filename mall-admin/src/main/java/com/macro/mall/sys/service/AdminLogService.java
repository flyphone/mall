package com.macro.mall.sys.service;

import com.macro.mall.model.SystemOperationLog;

import java.util.List;

/**
 * 
 *
 * @author zscat
 * @email 951449465@qq.com
 * @date 2019-04-03 17:43:32
 */
public interface AdminLogService {


    int createAdminLog(SystemOperationLog adminLog);



    List<SystemOperationLog> listAdminLog(SystemOperationLog adminLog, int pageNum, int pageSize);

    SystemOperationLog getAdminLog(Long id);

   
}
