package com.macro.mall.sys.controller;

import com.macro.mall.annotation.SysLog;
import com.macro.mall.dto.CommonResult;
import com.macro.mall.model.SystemOperationLog;
import com.macro.mall.sys.service.AdminLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 
 *
 * @author zscat
 * @email 951449465@qq.com
 * @date 2019-04-03 17:43:32
 */
@Controller
@Api(tags = "AdminLogController", description = "管理")
@RequestMapping("/sys/adminLog")
public class AdminLogController {
    @Resource
    private AdminLogService adminLogService;

    @SysLog(MODULE = "sys", REMARK = "添加")
    @ApiOperation(value = "添加")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:adminLog:create')")
    public Object create(@Validated @RequestBody SystemOperationLog sysAdminLog, BindingResult result) {
        CommonResult commonResult;
        int count = adminLogService.createAdminLog(sysAdminLog);
        if (count == 1) {
            commonResult = new CommonResult().success(count);
        } else {
            commonResult = new CommonResult().failed();
        }
        return commonResult;
    }


    @SysLog(MODULE = "sys", REMARK = "根据分页获取列表")
    @ApiOperation(value = "根据名称分页获取列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:adminLog:read')")
    public Object getList(SystemOperationLog adminLog,
                          @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                          @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize) {
        return new CommonResult().pageSuccess(adminLogService.listAdminLog(adminLog, pageNum, pageSize));
    }
    @SysLog(MODULE = "sys", REMARK = "根据编号查询信息")
    @ApiOperation(value = "根据编号查询信息")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:adminLog:read')")
    public Object getItem(@PathVariable("id") Long id) {
        return new CommonResult().success(adminLogService.getAdminLog(id));
    }

}
