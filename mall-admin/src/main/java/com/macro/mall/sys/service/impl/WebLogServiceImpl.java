package com.macro.mall.sys.service.impl;

import com.github.pagehelper.PageHelper;
import com.macro.mall.mapper.WebOperationLogMapper;
import com.macro.mall.model.SystemOperationLog;
import com.macro.mall.sys.service.WebLogService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 商品品牌Service实现类
 * Created by macro on 2018/4/26.
 */
@Service
public class WebLogServiceImpl implements WebLogService {
    @Resource
    private WebOperationLogMapper webLogMapper;
    


    @Override
    public int createWebLog(SystemOperationLog webLog) {
        return webLogMapper.insertSelective(webLog);
    }



    @Override
    public List<SystemOperationLog> listWebLog(SystemOperationLog webLog, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        return webLogMapper.queryList(webLog);

    }

    @Override
    public SystemOperationLog getWebLog(Long id) {
        return webLogMapper.selectByPrimaryKey(id);
    }

   
}
