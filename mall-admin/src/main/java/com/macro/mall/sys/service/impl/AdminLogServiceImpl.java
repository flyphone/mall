package com.macro.mall.sys.service.impl;

import com.github.pagehelper.PageHelper;
import com.macro.mall.mapper.AdminOperationLogMapper;
import com.macro.mall.model.SystemOperationLog;
import com.macro.mall.sys.service.AdminLogService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 商品品牌Service实现类
 * Created by macro on 2018/4/26.
 */
@Service
public class AdminLogServiceImpl implements AdminLogService {
    @Resource
    private AdminOperationLogMapper adminLogMapper;
    


    @Override
    public int createAdminLog(SystemOperationLog adminLog) {
        return adminLogMapper.insertSelective(adminLog);
    }



    @Override
    public List<SystemOperationLog> listAdminLog(SystemOperationLog adminLog, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        return adminLogMapper.queryList(adminLog);

    }

    @Override
    public SystemOperationLog getAdminLog(Long id) {
        return adminLogMapper.selectByPrimaryKey(id);
    }

   
}
