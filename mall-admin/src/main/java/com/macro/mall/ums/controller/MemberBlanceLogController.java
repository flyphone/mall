package com.macro.mall.ums.controller;

import com.macro.mall.dto.CommonResult;
import com.macro.mall.ums.model.UmsMemberBlanceLog;

import com.macro.mall.ums.service.MemberBlanceLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

/**
 * 
 *
 * @author zscat
 * @email 951449465@qq.com
 * @date 2019-04-01 18:44:20
 */
@Controller
@Api(tags = "MemberBlanceLogController", description = "管理")
@RequestMapping("/ums/memberBlanceLog")
public class MemberBlanceLogController {
    @Resource
    private MemberBlanceLogService memberBlanceLogService;


    @ApiOperation(value = "添加")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    @PreAuthorize("hasAuthority('ums:memberBlanceLog:create')")
    public Object create(@Validated @RequestBody UmsMemberBlanceLog umsMemberBlanceLog, BindingResult result) {
        CommonResult commonResult;
        int count = memberBlanceLogService.createMemberBlanceLog(umsMemberBlanceLog);
        if (count == 1) {
            commonResult = new CommonResult().success(count);
        } else {
            commonResult = new CommonResult().failed();
        }
        return commonResult;
    }

    @ApiOperation(value = "更新")
    @RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
    @ResponseBody
    @PreAuthorize("hasAuthority('ums:memberBlanceLog:update')")
    public Object update(@PathVariable("id") Long id,
                         @Validated @RequestBody UmsMemberBlanceLog umsMemberBlanceLog,
                         BindingResult result) {
        CommonResult commonResult;
        int count = memberBlanceLogService.updateMemberBlanceLog(id, umsMemberBlanceLog);
        if (count == 1) {
            commonResult = new CommonResult().success(count);
        } else {
            commonResult = new CommonResult().failed();
        }
        return commonResult;
    }

    @ApiOperation(value = "删除")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    @ResponseBody
    @PreAuthorize("hasAuthority('ums:memberBlanceLog:delete')")
    public Object delete(@PathVariable("id") Long id) {
        int count = memberBlanceLogService.deleteMemberBlanceLog(id);
        if (count == 1) {
            return new CommonResult().success(null);
        } else {
            return new CommonResult().failed();
        }
    }

    @ApiOperation(value = "根据名称分页获取列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    @PreAuthorize("hasAuthority('ums:memberBlanceLog:read')")
    public Object getList(UmsMemberBlanceLog memberBlanceLog,
                          @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                          @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize) {
        return new CommonResult().pageSuccess(memberBlanceLogService.listMemberBlanceLog(memberBlanceLog, pageNum, pageSize));
    }

    @ApiOperation(value = "根据编号查询信息")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    @PreAuthorize("hasAuthority('ums:memberBlanceLog:read')")
    public Object getItem(@PathVariable("id") Long id) {
        return new CommonResult().success(memberBlanceLogService.getMemberBlanceLog(id));
    }

}
