package com.macro.mall.ums.controller;

import com.macro.mall.dto.CommonResult;
import com.macro.mall.ums.model.TSchool;
import com.macro.mall.ums.service.SchoolService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 
 *
 * @author zscat
 * @email 951449465@qq.com
 * @date 2019-04-01 18:43:58
 */
@Controller
@Api(tags = "SchoolController", description = "管理")
@RequestMapping("/t/school")
public class SchoolController {
    @Resource
    private SchoolService schoolService;


    @ApiOperation(value = "添加")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    @PreAuthorize("hasAuthority('t:school:create')")
    public Object create(@Validated @RequestBody TSchool tSchool, BindingResult result) {
        CommonResult commonResult;
        int count = schoolService.createSchool(tSchool);
        if (count == 1) {
            commonResult = new CommonResult().success(count);
        } else {
            commonResult = new CommonResult().failed();
        }
        return commonResult;
    }

    @ApiOperation(value = "更新")
    @RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
    @ResponseBody
    @PreAuthorize("hasAuthority('t:school:update')")
    public Object update(@PathVariable("id") Long id,
                         @Validated @RequestBody TSchool tSchool,
                         BindingResult result) {
        CommonResult commonResult;
        int count = schoolService.updateSchool(id, tSchool);
        if (count == 1) {
            commonResult = new CommonResult().success(count);
        } else {
            commonResult = new CommonResult().failed();
        }
        return commonResult;
    }

    @ApiOperation(value = "删除")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    @ResponseBody
    @PreAuthorize("hasAuthority('t:school:delete')")
    public Object delete(@PathVariable("id") Long id) {
        int count = schoolService.deleteSchool(id);
        if (count == 1) {
            return new CommonResult().success(null);
        } else {
            return new CommonResult().failed();
        }
    }

    @ApiOperation(value = "根据名称分页获取列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    @PreAuthorize("hasAuthority('t:school:read')")
    public Object getList(TSchool school,
                          @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                          @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize) {
        return new CommonResult().pageSuccess(schoolService.listSchool(school, pageNum, pageSize));
    }

    @ApiOperation(value = "根据编号查询信息")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    @PreAuthorize("hasAuthority('t:school:read')")
    public Object getItem(@PathVariable("id") Long id) {
        return new CommonResult().success(schoolService.getSchool(id));
    }

}
