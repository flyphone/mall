package com.macro.mall.ums.service;

import com.macro.mall.ums.model.TSchool;

import java.util.List;


/**
 * 
 *
 * @author zscat
 * @email 951449465@qq.com
 * @date 2019-04-01 18:43:58
 */
public interface SchoolService {


    int createSchool(TSchool school);
    
    int updateSchool(Long id, TSchool school);

    int deleteSchool(Long id);



    List<TSchool> listSchool(TSchool school, int pageNum, int pageSize);

    TSchool getSchool(Long id);

   
}
