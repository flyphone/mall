package com.macro.mall.ums.service.impl;

import com.github.pagehelper.PageHelper;
import com.macro.mall.ums.mapper.MemberLevelMapper;
import com.macro.mall.ums.model.UmsMemberLevel;
import com.macro.mall.ums.service.MemberLevelService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 商品品牌Service实现类
 * Created by macro on 2018/4/26.
 */
@Service
public class MemberLevelServiceImpl implements MemberLevelService {
    @Resource
    private MemberLevelMapper memberLevelMapper;
    


    @Override
    public int createMemberLevel(UmsMemberLevel memberLevel) {
        return memberLevelMapper.insert(memberLevel);
    }

    @Override
    public int updateMemberLevel(Long id, UmsMemberLevel memberLevel) {
        memberLevel.setId(id);
        return memberLevelMapper.updateByPrimaryKeySelective(memberLevel);
    }

    @Override
    public int deleteMemberLevel(Long id) {
        return memberLevelMapper.deleteByPrimaryKey(id);
    }


    @Override
    public List<UmsMemberLevel> listMemberLevel(UmsMemberLevel memberLevel, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        return memberLevelMapper.list(memberLevel);

    }

    @Override
    public UmsMemberLevel getMemberLevel(Long id) {
        return memberLevelMapper.selectByPrimaryKey(id);
    }

   
}
