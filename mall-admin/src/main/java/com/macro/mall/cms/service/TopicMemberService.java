package com.macro.mall.cms.service;

import com.macro.mall.cms.model.CmsTopicMember;


import java.util.List;

/**
 * 
 *
 * @author zscat
 * @email 951449465@qq.com
 * @date 2019-04-04 14:04:34
 */
public interface TopicMemberService {


    int createTopicMember(CmsTopicMember topicMember);
    
    int updateTopicMember(Long id, CmsTopicMember topicMember);

    int deleteTopicMember(Long id);



    List<CmsTopicMember> listTopicMember(CmsTopicMember topicMember, int pageNum, int pageSize);

    CmsTopicMember getTopicMember(Long id);

   
}
