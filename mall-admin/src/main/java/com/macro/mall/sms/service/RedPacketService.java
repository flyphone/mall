package com.macro.mall.sms.service;

import com.macro.mall.sms.model.SmsRedPacket;


import java.util.List;

/**
 * 红包
 *
 * @author zscat
 * @email 951449465@qq.com
 * @date 2019-04-05 16:20:35
 */
public interface RedPacketService {


    int createRedPacket(SmsRedPacket redPacket);
    
    int updateRedPacket(Integer id, SmsRedPacket redPacket);

    int deleteRedPacket(Integer id);



    List<SmsRedPacket> listRedPacket(SmsRedPacket redPacket, int pageNum, int pageSize);

    SmsRedPacket getRedPacket(Integer id);


    int acceptRedPacket(Integer id);
}
