package com.macro.mall.sms.controller;

import com.macro.mall.annotation.SysLog;
import com.macro.mall.dto.CommonResult;
import com.macro.mall.sms.model.SmsRedPacket;
import com.macro.mall.sms.service.RedPacketService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 红包
 *
 * @author zscat
 * @email 951449465@qq.com
 * @date 2019-04-05 16:20:35
 */
@Controller
@Api(tags = "RedPacketController", description = "红包管理")
@RequestMapping("/sms/redPacket")
public class RedPacketController {
    @Resource
    private RedPacketService redPacketService;

    @SysLog(MODULE = "sms", REMARK = "添加红包")
    @ApiOperation(value = "添加红包")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    @PreAuthorize("hasAuthority('sms:redPacket:create')")
    public Object create(@Validated @RequestBody SmsRedPacket smsRedPacket, BindingResult result) {
        CommonResult commonResult;
        int count = redPacketService.createRedPacket(smsRedPacket);
        if (count == 1) {
            commonResult = new CommonResult().success(count);
        } else {
            commonResult = new CommonResult().failed();
        }
        return commonResult;
    }
    @SysLog(MODULE = "sms", REMARK = "更新红包")
    @ApiOperation(value = "更新红包")
    @RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
    @ResponseBody
    @PreAuthorize("hasAuthority('sms:redPacket:update')")
    public Object update(@PathVariable("id") Integer id,
                         @Validated @RequestBody SmsRedPacket smsRedPacket,
                         BindingResult result) {
        CommonResult commonResult;
        int count = redPacketService.updateRedPacket(id, smsRedPacket);
        if (count == 1) {
            commonResult = new CommonResult().success(count);
        } else {
            commonResult = new CommonResult().failed();
        }
        return commonResult;
    }
    @SysLog(MODULE = "sms", REMARK = "删除红包")
    @ApiOperation(value = "删除红包")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    @ResponseBody
    @PreAuthorize("hasAuthority('sms:redPacket:delete')")
    public Object delete(@PathVariable("id") Integer id) {
        int count = redPacketService.deleteRedPacket(id);
        if (count == 1) {
            return new CommonResult().success(null);
        } else {
            return new CommonResult().failed();
        }
    }
    @SysLog(MODULE = "sms", REMARK = "根据红包分页获取红包列表")
    @ApiOperation(value = "根据红包名称分页获取红包列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    @PreAuthorize("hasAuthority('sms:redPacket:read')")
    public Object getList(SmsRedPacket redPacket,
                          @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                          @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize) {
        return new CommonResult().pageSuccess(redPacketService.listRedPacket(redPacket, pageNum, pageSize));
    }
    @SysLog(MODULE = "sms", REMARK = "根据编号查询红包信息")
    @ApiOperation(value = "根据编号查询红包信息")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    @PreAuthorize("hasAuthority('sms:redPacket:read')")
    public Object getItem(@PathVariable("id") Integer id) {
        return new CommonResult().success(redPacketService.getRedPacket(id));
    }

    @SysLog(MODULE = "sms", REMARK = "领取红包")
    @ApiOperation(value = "领取红包")
    @RequestMapping(value = "/accept/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Object accept(@PathVariable("id") Integer id) {
        int count = redPacketService.acceptRedPacket(id);
        if (count == 1) {
            return new CommonResult().success("领取成功");
        } else {
            return new CommonResult().failed("你已经领取此红包");
        }
    }
}
